"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import pymongo

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_new" methods=['POST'])
def new():
    km = request.args.get('km', 999, type=float)
    dist = request.form['distance']
    begin_date = request.form['begin_date']
    begin_time = request.form['begin_time']
    open_time = request.form['open']
    close_time = request.form['close']

    item_doc = {
            'km': request.args.get('km', 999, type=float)
            'distance': request.form['distance']
            'begin date': request.form['begin_date']
            'begin time': request.form['begin_time']
            'open time': request.form['begin_time']
            'close_time': request.form['close']
            }

    db.brevetdb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route("/display")
def display():
    _items = db.brevet.find()
    items = [item for item in _items]

    return render_template('display.html', items=items)

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request for km")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    app.logger.debug("Got a JSON request for brevet distance")
    brevet_dist = request.args.get('distance', 1000, type=float)
    app.logger.debug("distance={}".format(brevet_dist))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, brevet_dist, arrow.now().isoformat())
    close_time = acp_times.close_time(km, brevet_dist, arrow.now().isoformat())
    #open_time = acp_times.open_time(km, 200, arrow.now().isoformat())
    #close_time = acp_times.close_time(km, 200, arrow.now().isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
